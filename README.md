# Carousel Vanilla JS #

This is a carousel library which could be integrated with html based app.
Written in es6.
Transpiled using babel.
Webpack does the heavylifting and the messy jobs.

### How do I see the sample? ###

* ``` npm i ``` (Install all dependencies)
* ``` npm start ``` (Start a dev-server to hosting static assets)

Now navigate to http://localhost:8080

### Usage ###

You would need to link carousel.js and carousel.css present in the lib folder to your html.

And inside your script you should create a new object of Carousel.

new Carousel(options);

### Options ###

```
width: '30em', // Width of carousel
height: '30em', // Height of carousel
hover: true, // If true, pauses on hover
keyboard: true, // If true, on paused, navigate using arrow keys
loop: true, // TBD
duration: 1000, // Duration of each transition
ajax: false // If data comes from an api, use `ajax: {url: url, hook: yourHook}  | If false, you can embed content/slides of carousel manually`
```

## Hook ##
The hook in options.ajax should return a string. The returned value is the html which would be rendered in the inner body of the carousel.
The hook is used to provide full power to the developer to customise the content based on the response from the api.