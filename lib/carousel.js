/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DEFAULTS = {
    width: '30em',
    height: '30em',
    loop: true,
    duration: 1000,
    ajax: false
};

var Carousel = function () {
    function Carousel(element, options) {
        _classCallCheck(this, Carousel);

        this.config = Object.assign({}, DEFAULTS, options);
        this.element = document.querySelector(element);

        if (!element) {
            throw new Error('Carousel: element => ' + element + ' is missing');
        }

        if (!this.config.ajax.url) {
            this.setup();
        } else {
            this.ajaxSetup();
        }
    }

    _createClass(Carousel, [{
        key: 'setup',
        value: function setup() {

            this.element.style.width = this.config.width;
            this.element.style.height = this.config.height;

            this.nodes = this.element.querySelectorAll('.carousel-inner li');
            this.current = -1;

            // Start looping
            this.play();

            // Add event listeners
            addEventListeners();
        }
    }, {
        key: 'addEventListeners',
        value: function addEventListeners() {
            var _this = this;

            this.element.addEventListener('mouseenter', function (event) {
                return _this.pause();
            });
            this.element.addEventListener('mouseleave', function (event) {
                return _this.play();
            });

            document.addEventListener('keydown', function (event) {
                switch (event.code) {
                    case "ArrowLeft":
                        _this.pause();_this.prev();break;
                    case "ArrowRight":
                        _this.pause();_this.next();break;
                }
            });
        }
    }, {
        key: 'ajaxSetup',
        value: function ajaxSetup() {
            var _this2 = this;

            if (this.config.ajax.url && typeof this.config.ajax.url === 'string') {

                if (!this.config.ajax.url) {
                    throw new Error('Carousel: ajax.url is missing');
                }

                if (!this.config.ajax.hook) {
                    throw new Error('Carousel: ajax.hook is missing');
                }

                this.httpFetch(this.config.ajax.url).then(function (res) {
                    return res.json();
                }).then(this.config.ajax.hook).then(function (html) {
                    _this2.render(html);

                    _this2.setup();
                }).catch(function (err) {
                    console.error('Carousel: ' + err);
                });
            }
        }
    }, {
        key: 'httpFetch',
        value: function httpFetch() {

            return fetch(this.config.ajax.url);
        }
    }, {
        key: 'render',
        value: function render(html) {
            this.element.insertAdjacentHTML('afterbegin', html);
        }
    }, {
        key: 'setActive',
        value: function setActive(index) {
            var list = this.element.querySelectorAll('.carousel-inner li');
            var target = list[index];
            var activeElement = list[this.current];
            if (activeElement && activeElement.classList.contains('current')) {
                activeElement.classList.remove('current');
            }
            target.classList.add('current');
            this.current = index;
        }
    }, {
        key: 'play',
        value: function play() {
            var _this3 = this;

            this._interval = setInterval(function () {
                _this3.next();
            }, this.config.duration);
        }
    }, {
        key: 'pause',
        value: function pause() {
            clearInterval(this._interval);
        }
    }, {
        key: 'next',
        value: function next() {
            if (this.current >= -1 && this.current < this.nodes.length - 1) {
                this.setActive(this.current + 1);
            } else {
                this.setActive(0);
            }
        }
    }, {
        key: 'prev',
        value: function prev() {
            if (this.current >= 1 && this.current <= this.nodes.length - 1) {
                this.setActive(this.current - 1);
            } else {
                this.setActive(this.nodes.length - 1);
            }
        }
    }]);

    return Carousel;
}();

exports.default = Carousel;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _carousel = __webpack_require__(0);

var _carousel2 = _interopRequireDefault(_carousel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

__webpack_require__(1);

window.Carousel = _carousel2.default;

/***/ })
/******/ ]);