const DEFAULTS = {
    width: '30em',
    height: '30em',
    hover: true,
    keyboard: true,
    loop: true,
    duration: 1000,
    ajax: false
}

class Carousel {

    constructor(element, options) {
        this.config = Object.assign({}, DEFAULTS, options);
        this.element = document.querySelector(element);

        if(!element) {
            throw new Error(`Carousel: element => ${element} is missing`);
        }

        if (!this.config.ajax.url) {
            this.setup();
        } else {
            this.ajaxSetup();
        }
    }

    setup() {

        this.element.style.width = this.config.width;
        this.element.style.height = this.config.height;

        this.nodes = this.element.querySelectorAll('.carousel-inner li');
        this.current = -1;

        // Start looping
        this.play();

        // Add event listeners
        this.addEventListeners();
        
    }

    addEventListeners() {

        if(this.config.hover) {
            this.element.addEventListener('mouseenter', (event) => this.pause());
            this.element.addEventListener('mouseleave', (event) => this.play());
        }

        if(this.config.keyboard) {
            document.addEventListener('keydown', (event) => {
                switch(event.code) {
                    case "ArrowLeft": this.pause(); this.prev(); break;
                    case "ArrowRight": this.pause(); this.next(); break;
                }
            });
        }
    }

    ajaxSetup() {

        if(this.config.ajax.url && typeof this.config.ajax.url === 'string') {

            if(!this.config.ajax.url) {
               throw new Error(`Carousel: ajax.url is missing`); 
            }

            if(!this.config.ajax.hook) {
               throw new Error(`Carousel: ajax.hook is missing`); 
            }

            this.httpFetch(this.config.ajax.url)
                .then( res => res.json())
                .then(this.config.ajax.hook)
                .then( html => {
                    this.render(html);

                    this.setup();

                }).catch( err => {
                    console.error(`Carousel: ${err}`);
                });
        }
    }

    httpFetch() {

        return fetch(this.config.ajax.url);
    }

    render(html) {
        this.element.insertAdjacentHTML('afterbegin', html);
    }
  
    setActive(index) {
        let list = this.element.querySelectorAll('.carousel-inner li');
        let target = list[index];
        let activeElement = list[this.current];
        if(activeElement && activeElement.classList.contains('current')) {
            activeElement.classList.remove('current');            
        }
        target.classList.add('current');
        this.current = index;
    }

    play() {
        
        this._interval = setInterval(() => {
            this.next();
        }, this.config.duration);
        
    }

    pause() {
        clearInterval(this._interval);
    }

    next() {
        if(this.current >= -1 && this.current < this.nodes.length - 1) {
            this.setActive(this.current + 1);
        } else {
            this.setActive(0);
        }
    }

    prev() {
        if(this.current >= 1 && this.current <= this.nodes.length - 1) {
            this.setActive(this.current - 1);
        } else {
            this.setActive(this.nodes.length - 1);
        }
    }
}

export default Carousel;